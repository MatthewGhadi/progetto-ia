:- lib(fd).
:- lib(gra_alg).

constraint(VariablesList,InequalitiesList,Graph,NumberOfNodes):-
    %devo cambiare la sola parte del grafo che cambia da un'invocazione all'altra
    arg(2, Graph, AdjEdges),
    length(VariablesList, Len),
    dim(SourceAdjEdges,[Len]),
    dim(NewAdjEdges,[NumberOfNodes]),
    check_domain_edges(VariablesList,AdjEdges,NewAdjEdges,SourceAdjEdges,1,NumberOfNodes),
    update_struct(graph, [adj:NewAdjEdges], Graph, NewGraph),
    %passo la struttura Graph all'alogritmo di shortest_path con il check per cicli negativi
    shortest_paths_bellman_ford(NewGraph,0,NumberOfNodes,Paths),
    %trasformo l'array in lista
    array_list(Paths,PathsList),
    %aggiorno i domini delle variabili con il nuovo peso
    domain_update(VariablesList,PathsList,NewGraph,NumberOfNodes,1),
    (ground(VariablesList) -> true;suspend(constraint(VariablesList,InequalitiesList,NewGraph,NumberOfNodes), 1, [VariablesList -> fd:min, VariablesList -> fd:max])).

check_domain_edges([],_,NewAdjEdges,SourceAdjEdges,_,SourceNode):-
    array_list(SourceAdjEdges,SourceList),
    arg(SourceNode,NewAdjEdges,SourceList).

check_domain_edges([Var|VariablesListTail],AdjEdges,NewAdjEdges,SourceAdjEdges,CurrentNode,SourceNode):-
    dvar_domain(Var,VarDomain),
    dom_range(VarDomain,Min,Max),
    NewMin is -Min,
    A=e(CurrentNode,SourceNode,NewMin),
    arg(CurrentNode,AdjEdges,OldHead),
    %creo la nuova lista per il nodo corrente, sostituendo solamente l'edge che cambia
    generate_new_head(OldHead,NewHead,A),
    arg(CurrentNode,NewAdjEdges,NewHead),
    %popolo l'array degli edge del source node
    arg(CurrentNode,SourceAdjEdges,e(SourceNode,CurrentNode,Max)),
    NextNode is CurrentNode + 1,
    check_domain_edges(VariablesListTail,AdjEdges,NewAdjEdges,SourceAdjEdges,NextNode,SourceNode).

generate_new_head([],[],_).
generate_new_head([e(A,B,OldMin)|OldList],[e(A,B,NewMin)|NewList],e(A,B,NewMin)):- !,
    generate_new_head(OldList,NewList,e(A,B,NewMin)).
generate_new_head([e(A,C,OldMin)|OldList],[e(A,C,OldMin)|NewList],e(A,B,NewMin)):- !,
    generate_new_head(OldList,NewList,e(A,B,NewMin)).

add_domains_edges([],InequalitiesList,_,_,InequalitiesList).
add_domains_edges([Var|VariablesListTail],[A,B|GraphListTail],SourceNode,CurrentNode,InequalitiesList):-
    %%utilizzo Count in quanto il grafo istanzia la variabile Var con la label numerica corrispondente
    dvar_domain(Var,VarDomain),
    dom_range(VarDomain,Min,Max),
    NewMin is -Min,
    A=e(CurrentNode,SourceNode,NewMin),
    B=e(SourceNode,CurrentNode,Max),
    NextNode is CurrentNode + 1,
    add_domains_edges(VariablesListTail,GraphListTail,SourceNode,NextNode,InequalitiesList).

extract_value(Min-_,NewMin):-
    NewMin is -Min.

domain_update([],_,_,_,_).
domain_update([Var|VariablesListTail],[NewMax-_|PathsTail],Graph,SourceNode,Count):-
    %%utilizzo Count in quanto il grafo istanzia la variabile Var con la label numerica corrispondente
    ( var(Var) 
    ->  single_pair_shortest_path_bellman_ford(Graph, 0, Count, SourceNode, Path),
        extract_value(Path,NewMin),
        dvar_remove_greater(Var, NewMax),
        dvar_remove_smaller(Var, NewMin)
    ; 
        true
    ),
    C1 is Count + 1,
    domain_update(VariablesListTail,PathsTail,Graph,SourceNode,C1).