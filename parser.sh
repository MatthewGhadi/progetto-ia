#!/bin/sh

#controllo se il numero di argomenti è corretto
if test $# != 4
then 
	echo "Errore: numero errato di argomenti"
    echo "Uso corretto: parser.sh instance_file output_var_file output_diseq_file output_ecl_file"
	exit 1
fi

#inizializzo le variabili
INPUTFILE=$1
VAROUTPUT=$2
DISEQOUTPUT=$3
ECLOUTPUT=$4
A=0
B=0
NVAR=0
SOURCENODE=0

#estraggo dal file di input i valori che dovrò mettere nel fatto e(_,_,_). per gli edges del grafo
> "tempdiseq.txt"

for LINE in $(cat $INPUTFILE)
do 
    cut -d " " -f 3 | tr -d 'X' | tr ' ' ',' >> "tempdiseq.txt"
done < $INPUTFILE

> "tempdiseq2.txt"

for LINE in $(cat "tempdiseq.txt")
do 
    cut -d " " -f 1,5 | tr -d 'X' | tr ' ' ',' >> "tempdiseq2.txt"
done < $INPUTFILE

#li incollo nell'ordine giusto
paste -d "," "tempdiseq.txt" "tempdiseq2.txt" > "tempdiseq3.txt"

> "tempdiseq4.txt" 

#creo il file di fatti
for LN in $(cat "tempdiseq3.txt")
do
    echo "e($LN)." | tr -d '\r' >> "tempdiseq4.txt"
done

> "tempvar.txt"

#estraggo dal file di input tutte le variabili in modo non ripetuto ordinate numericamente
cut -d " " -f 1,3 < $INPUTFILE | tr '\n' ' '| tr -d 'X' | xargs -n1 | sort -u -n > "tempvar.txt"

#estraggo il nodo sorgente che sarà uguale al numero di variabili più 1
NVAR=$(cat tempvar.txt | wc -l )
SOURCENODE=$(($NVAR + 1))

#creo il file di variabili con i domini randomici
> $VAROUTPUT

for LN in $(cat "tempvar.txt")
do
    A=-$((RANDOM%150))
    A=$(($A-50))
    B=$((RANDOM%200))
    B=$(($B+50))
    echo "X$LN.\t[$(($A))..$(($B))]." >> $VAROUTPUT
    #echo "e($(($LN)),$(($SOURCENODE)),$((-$A)))." >> "tempdiseq4.txt"
    #echo "e($(($SOURCENODE)),$(($LN)),$(($B)))." >> "tempdiseq4.txt"
done

echo ":- lib(fd)." > $ECLOUTPUT
echo "impose_constraint(VariablesList):-" >> $ECLOUTPUT

for I in $(seq $NVAR)
do
    echo "nth1($I,VariablesList,Var$I)," >> $ECLOUTPUT
done

> "temp.txt"

#Sostituisco tutte le X con Var
sed "s/X/Var/g" $INPUTFILE >> "temp.txt"

> "temp2.txt"

#Aggiungo tutti i cancelletti prima dell'uguale
sed "s/=</#=</g" "temp.txt" >> "temp2.txt"

#Aggiungo tutte le virgole in fondo
sed "$!s/$/,/" "temp2.txt" >> $ECLOUTPUT

echo "true." >> $ECLOUTPUT

#appendo in testa ai file il numero di linee
echo "$(cat tempvar.txt | wc -l | tr -d '\r').\n$(cat $VAROUTPUT)" > $VAROUTPUT

echo "$(cat tempdiseq4.txt | wc -l | tr -d '\r').\n$(cat tempdiseq4.txt)" > $DISEQOUTPUT

#rimuovo i file temporanei
rm "tempdiseq.txt" "tempdiseq2.txt" "tempdiseq3.txt" "tempdiseq4.txt" "tempvar.txt" "temp.txt" "temp2.txt"

echo "File variabili creato: $VAROUTPUT"
echo "File disequazioni creato: $DISEQOUTPUT"
echo "File .ecl creato $ECLOUTPUT"
