:- lib(fd).
impose_constraint(VariablesList):-
nth1(1,VariablesList,Var1),
nth1(2,VariablesList,Var2),
nth1(3,VariablesList,Var3),
Var1 - Var2 #=< 4,
Var2 - Var3 #=< 7,
Var1 - Var3 #=< 8,
true.
