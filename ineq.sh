#!/bin/sh

#Sostituisco tutte le X con Var
#Aggiungo tutti i cancelletti prima dell'uguale
#Aggiungo tutte le virgole in fondo

#controllo se il numero di argomenti è corretto
if test $# != 2
then 
	echo "Errore: numero errato di argomenti"
    echo "Uso corretto: ineq.sh instance_file output_file"
	exit 1
fi
#inizializzo le variabili
INPUTFILE=$1
ECLOUTPUT=$2


echo ":- lib(fd)." > $ECLOUTPUT
echo "impose_constraint(VariablesList):-" >> $ECLOUTPUT

> "temp.txt"

sed "s/X/Var/g" $INPUTFILE >> "temp.txt"

> "temp2.txt"

sed "s/=</#=</g" "temp.txt" >> "temp2.txt"

sed "$!s/$/,/" "temp2.txt" >> $ECLOUTPUT

echo "true." >> $ECLOUTPUT

#rimuovo i file temporanei
#rm "temp.txt" "temp2.txt"

echo "File creato $ECLOUTPUT"
