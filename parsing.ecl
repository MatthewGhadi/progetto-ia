%%% FILE PARSING %%%
get_file_lines(File, Terms) :-
        open(File, read, S),
        read(S,Count),
        stream_get_terms(S, Terms, Count),
        close(S).

stream_get_terms(S,[],0):- ! .
stream_get_terms(S, [Term|TermsTail], Count) :-
    read(S,Term),
    C1 is Count - 1,
    stream_get_terms(S, TermsTail, C1).
    
get_variables_lines(File, Terms) :-
        open(File, read, S),
        read(S,Count),
        stream_get_domains(S, Terms, Count),
        close(S).

stream_get_domains(S,[],0):- ! .
stream_get_domains(S, [Term|TermsTail], Count) :-
    read(S,Term),
    read(S,DomList),
    Term :: DomList,
    C1 is Count - 1,
    stream_get_domains(S, TermsTail, C1).
