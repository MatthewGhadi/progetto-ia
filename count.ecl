:- lib(fd).
:- ["csp.ecl"].

count(A,B):-
    findall(.,csp(A,B),L),
    length(L,Lung),
    writeln(Lung).