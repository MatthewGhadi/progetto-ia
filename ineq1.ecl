:- lib(fd).
impose_constraint(VariablesList):-
nth1(1,VariablesList,Var1),
nth1(2,VariablesList,Var2),
nth1(3,VariablesList,Var3),
nth1(4,VariablesList,Var4),
nth1(5,VariablesList,Var5),
nth1(6,VariablesList,Var6),
Var1 - Var2 #=< 1,
Var1 - Var4 #=< -4,
Var2 - Var3 #=< 2,
Var2 - Var5 #=< 7,
Var2 - Var6 #=< 5,
Var3 - Var6 #=< 10,
Var4 - Var2 #=< 2,
Var5 - Var1 #=< -1,
Var5 - Var4 #=< 3,
Var6 - Var3 #=< 8,
true.
