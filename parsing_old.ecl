trasforma(X,Ineq):- 
    get_lines(Ineq, ListOfIneq), %devo trasformare la lista di inequalities in una lista di liste
    split_token(ListOfIneq,ListOfIneqToken),
    writeln(ListOfIneqToken),
    split_token(ListOfIneqToken,L3),
    writeln(L3).

%%%% Le seguenti due regole permettono di parsare un file e trasformarlo in lista di diseguaglianze
get_lines(File, Lines) :-
        open(File, read, S),
        stream_get_lines(S, Lines),
        close(S).

stream_get_lines(S, Lines) :-
    ( read_string(S, end_of_line, _, Line) ->
        Lines = [Line|Ls],
        stream_get_lines(S, Ls)
    ;
        Lines = []
    ).

split_inequalities([],[]).
split_inequalities([HeadIneq|TailIneq],[SubStrings|ListOfIneqToken]):-
    writeln(HeadIneq),
    split_string(HeadIneq, "", "-", SubStrings),
    split_inequalities(TailIneq,ListOfIneqToken).

split_token([],[]).
split_token([HeadIneq|TailIneq],[SubStrings|ListOfIneqToken]):-
    writeln(HeadIneq),
    term_string(HeadIneq, Var, variables(Vars)),
    split_token(TailIneq,ListOfIneqToken).

read_prova(Token,Class):-
    open("inequalities.txt",read,Stream),
    read_token(Stream, Token, Class),
    close(Stream).
