:- lib(fd).
:- lib(graph_algorithms).
:- lib(graphviz).
:- ["parsing.ecl"].

%% es. constraint("var2.txt","diseq2.txt").

constraint(VarInputFile,InequalitiesInputFile):- %% da sostituire con i file di input
    %effettuo il parsing dei files di input
    get_variables_lines(VarInputFile, VariablesList),
    get_file_lines(InequalitiesInputFile, InequalitiesList),
    writeln("VariablesList: "),
    writeln(VariablesList),
    writeln("InequalitiesList: "),
    writeln(InequalitiesList),
    NumberOfNodes is length(VariablesList)+1, %%NumberOfNodes è anche la label del SourceNode
    %utilizzo make_graph/3 per creare il grafo
        % NumberOfNodes è il numero di nodi del grafo, che sarà pari al numero di variabili +1
        % creare la struttura e(Source, Target, EdgeData)
        % mi restituisce la struttura graph
    make_graph(NumberOfNodes, InequalitiesList, Graph),
    writeln("Graph: "),
    writeln(Graph),
    %mediante la libreria graphviz salvo un'immagine del grafo in formato jpg
    write_graph(Graph,"mygraph",jpg),

    %% controllo se ci sono cicli
    ( graph_is_acyclic(Graph)
    ->  (bellman_ford(Graph,NumberOfNodes,VariablesList)) 
    ;   (graph_cycles(Graph,BreakingEdges), %%cerco gli edge che creano cicli
        writeln("BreakingEdges: "),
        writeln(BreakingEdges),
        (no_negative_cycles(Graph,BreakingEdges) %%cerco se ci sono cicli di costo negativo
        -> (bellman_ford(Graph,NumberOfNodes,VariablesList))
        ; ( writeln("\n"),
            writeln("There are no feasible solutions!"))))).

%% cerco gli shortest path tra i due nodi invertiti di ordine
%% faccio la somma tra il valore del BreakingEdege e lo shortest path trovato
%% se è negativo il problema non ha soluzioni
%% altrimenti vado avanti
no_negative_cycles(_,[]).
no_negative_cycles(Graph,[e(S,D,Weight)|BreakingEdgesTail]):-
    single_pair_shortest_path(Graph, 0, D, S, Path),
    %writeln("Path: "),
    %writeln(Path),
    is_negative(Path,Weight),!,
    no_negative_cycles(Graph,BreakingEdgesTail).

is_negative(MinLen-_,Weight):-
    MinLen + Weight >= 0.

bellman_ford(Graph,SourceNode,VariablesList):-
    %applico Bellman-Ford per computare gli shortest paths
    shortest_paths_bellman_ford(Graph,0,SourceNode,Paths),
    writeln("\nShortest Paths: "),
    writeln(Paths),
    array_list(Paths,PathsList),
    %aggiorno i domini delle variabili con il nuovo peso
    writeln("Vecchi Domini: "),
    writeln(VariablesList),
    domain_update(VariablesList,PathsList,Graph,SourceNode,1),
    writeln("Domini delle variabili aggiornati!"),
    writeln(VariablesList).

extract_value(Min-_,NewMin):-
    NewMin is -Min.

domain_update([],_,_,_,_).
domain_update([Var|VariablesListTail],[NewMax-_|PathsTail],Graph,SourceNode,Count):-
    %%utilizzo Count in quanto il grafo istanzia la variabile Var con la label numerica corrispondente
    single_pair_shortest_path_bellman_ford(Graph, 0, Count, SourceNode, Path),
    extract_value(Path,NewMin),
    TempVar::[NewMin..NewMax],
    dvar_domain(TempVar,NewDom),
    dvar_domain(Var,OldDom),
    dom_intersection(OldDom,NewDom,DomInt,_),
    dvar_update(Var,DomInt),
    C1 is Count + 1,
    domain_update(VariablesListTail,PathsTail,Graph,SourceNode,C1).