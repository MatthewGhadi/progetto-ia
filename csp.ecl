:- lib(fd).
%:- lib(fd_search).
:- lib(listut).
:- ["parsing.ecl"].
:- ["constraint.ecl"].
:- ["ineq.ecl"].

%% es. csp("var2.txt","diseq2.txt").

csp(VarInputFile,InequalitiesInputFile):-
    %effettuo il parsing dei files di input
    get_variables_lines(VarInputFile, VariablesList),
    get_file_lines(InequalitiesInputFile, InequalitiesList),

    %%Creo il grafo
    NumberOfNodes is length(VariablesList)+1, %%NumberOfNodes è anche la label del SourceNode
    add_domains_edges(VariablesList,GraphList,NumberOfNodes,1,InequalitiesList),
    %utilizzo make_graph/3 per creare il grafo
        % NumberOfNodes è il numero di nodi del grafo, che sarà pari al numero di variabili +1
        % creare la struttura e(Source, Target, EdgeData)
        % mi restituisce la struttura graph
    make_graph(NumberOfNodes, GraphList, Graph),

    constraint(VariablesList,InequalitiesList,Graph,NumberOfNodes),
    %impose_constraint(VariablesList),
    labeling(VariablesList),
    %search(VariablesList,0,input_order,indomain_random,complete,[]),
    writeln(VariablesList).